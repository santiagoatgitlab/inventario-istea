<?php
include("../header.php");
?>

<h2>Nuevo precio</h2>

<?php

if (!isset($_POST["precio"])){
$query      = "select id,nombre,simbolo from moneda";
$qry_result = mysqli_query($dblink,$query);
$monedas    = mysqli_fetch_all($qry_result,MYSQLI_ASSOC);
?>

<form action="" method="POST">
    <div class="campo">
        <label>Precio</label>
        <input type="text" name="precio" required />
    </div>
    <div class="campo">
        <label>Moneda</label>
		<select name="moneda" required>
		<?php foreach ($monedas as $moneda) { ?> 
			<option value="<?php echo $moneda["id"]; ?>"><?php echo $moneda["nombre"] . "(" . $moneda["simbolo"] . ")"; ?></option>
		<?php } ?>
		</select>
    </div>
	</br>
    <input type="submit" value="Enviar"/>
</form>
<?php }
else {
    $precio     = mysqli_real_escape_string ($dblink,$_POST["precio"]);
    $moneda_id	= mysqli_real_escape_string ($dblink,$_POST["moneda"]);
    $query      = "insert into precio (precio,moneda_id) 
					values (
					$precio,
					$moneda_id
					)";
    //echo $query . "</br>";
    $qry_result = mysqli_query  ($dblink,$query);
    if ($qry_result) {
        echo "Se creó el nuevo precio";
    } else {
        echo "Hubo un error, no se creó el precio";
    }
}
?>
<nav>
    <a href="/tp/inventario-istea/precios/listar.php">Volver al listado</a>
</nav>
<?php
include("../footer.php");
?>
