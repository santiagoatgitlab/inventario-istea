<?php
include("../header.php");

$search_line    = "";
$search_entry   = "";
if ( isset($_GET['search']) ){
    $search_entry = mysqli_real_escape_string ($dblink,$_GET["search"]);
    $search_line = ' where p.precio like "%' . $search_entry . '%" or m.nombre like "%' . $search_entry . '%"';
}

$query = "select p.id,p.precio,m.simbolo as moneda
    from precio p
    left join moneda m on m.id = p.moneda_id
    " . $search_line . " order by id";
$qry_result = mysqli_query  ($dblink,$query);
$precios = mysqli_fetch_all ($qry_result,MYSQLI_ASSOC); //extrae el array con los datos de la DB

if (is_null($precios)){
    $precios = [];
}
?>

<h1>Precios</h1>
<?php if (!empty($search_entry)) { ?>
<div class="search-results">
    <p>Resultados de la búsqueda: <span>"<?php echo $search_entry; ?>"</span></p>
    <a href="listar.php">X</a>
</div>
<?php } ?>
<nav class="top">
    <a href="/tp/inventario-istea/precios/insert.php">Nuevo precio</a>
    <form action="" method="get">
        <input type="text" name="search" />
        <input type="submit" value="Buscar" />
    </form>
</nav>
<table cellspacing="0" cellpadding="0">
    <tr> <!-- abrir fila -->
        <th>ID</th>   <!-- columna -->
        <th>Precio</th>
        <th>Acciones</th>
    </tr> <!-- cerrar fila -->
    <?php foreach ($precios as $precio) { ?>
    <tr>
        <td><?php echo $precio["id"]; ?></td>
        <td><?php echo $precio["moneda"] . " " . $precio["precio"]; ?></td>
        <td>
            <a href="/tp/inventario-istea/precios/editar.php?id=<?php echo $precio["id"]; ?>"><img src="/tp/inventario-istea/iconos/editar.png" width="20"/></a>
            <a onclick="return confirm('¿Seguro/a que desea eliminar el precio?')" href="/tp/inventario-istea/precios/eliminar.php?id=<?php echo $precio["id"]; ?>"><img src="/tp/inventario-istea/iconos/borrar.png" width="20"/></a>
        </td>
    </tr>
    <?php } ?>
</table>
<?php
include("../footer.php");
?>
