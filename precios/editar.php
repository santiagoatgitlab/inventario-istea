<?php
include("../header.php");
?>

<h2>Editar precio</h2>

<?php



$id = mysqli_real_escape_string($dblink,$_GET["id"]);



if (!isset($_POST["precio"])){
$query = "select p.id,p.precio,m.simbolo, p.moneda_id
    from precio p
    left join moneda m on m.id = p.moneda_id
    where p.id = $id
    order by p.id
    ";
            
$qry_result = mysqli_query  ($dblink,$query);
$precio = mysqli_fetch_array ($qry_result,MYSQLI_ASSOC); //extrae el array con los datos de la DB

$query      = "select id,nombre,simbolo from moneda";
$qry_result = mysqli_query($dblink,$query);
$monedas    = mysqli_fetch_all($qry_result,MYSQLI_ASSOC);
?>

<form action="" method="POST">
    <div class="campo">
        <label>Precio</label>
        <input type="text" name="precio" required value="<?php echo $precio["precio"]; ?>"/>
    </div>
    <div class="campo">
        <label>Moneda</label>
		<select name="moneda" required>
		<?php foreach ($monedas as $moneda) { ?> 
			<option value="<?php echo $moneda["id"]; ?>"<?php if ($precio["moneda_id"] == $moneda['id']) echo " selected"; ?>>
                <?php echo $moneda["nombre"] . "(" . $moneda["simbolo"] . ")"; ?>
            </option>
		<?php } ?>
		</select>
    </div>
	</br>
    <input type="submit" value="Enviar"/>
</form>
<?php }
else {
    $precio     = mysqli_real_escape_string ($dblink,$_POST["precio"]);
    $moneda_id	= mysqli_real_escape_string ($dblink,$_POST["moneda"]);
    $query      = "update precio set
            precio = $precio,
            moneda_id = $moneda_id
            where id = $id
        ";
    //echo $query . "</br>";
    $qry_result = mysqli_query  ($dblink,$query);
    if ($qry_result) {
        echo "Se actualizó el precio";
    } else {
        echo "Hubo un error, no se actualizó el precio";
    }
}
?>
<nav>
    <a href="/tp/inventario-istea/precios/listar.php">Volver al listado</a>
</nav>
<?php
include("../footer.php");
?>
