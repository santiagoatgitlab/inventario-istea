<?php
include("../header.php");

$search_line    = "";
$search_entry   = "";
if ( isset($_GET['search']) ){
    $search_entry = mysqli_real_escape_string ($dblink,$_GET["search"]);
    $search_line = ' where nombre like "%' . $search_entry . '%"';
}

$query = "select * from categoria" . $search_line;
$qry_result = mysqli_query  ($dblink,$query);
$categorias = mysqli_fetch_all ($qry_result,MYSQLI_ASSOC); //extrae el array con los datos de la DB
?>


<h1>Categorías</h1>
<?php if (!empty($search_entry)) { ?>
<div class="search-results">
    <p>Resultados de la búsqueda: <span>"<?php echo $search_entry; ?>"</span></p>
    <a href="listar.php">X</a>
</div>
<?php } ?>
<nav class="top">
    <a href="/tp/inventario-istea/categoria/insert.php">Nueva categoría</a>
    <form action="" method="get">
        <input type="text" name="search" />
        <input type="submit" value="Buscar" />
    </form>
</nav>
<table cellspacing="0" cellpadding="0">
    <tr> <!-- abrir fila -->
        <th>ID</th>   <!-- columna -->
        <th>Nombre</th>
        <th>Acciones</th>
    </tr> <!-- cerrar fila -->
    <?php foreach ($categorias as $categoria) { ?>
    <tr>
        <td><?php echo $categoria["id"]; ?></td>
        <td><?php echo $categoria["nombre"]; ?></td>
        <td>
            <a href="/tp/inventario-istea/categoria/editar.php?id=<?php echo $categoria["id"]; ?>"><img src="/tp/inventario-istea/iconos/editar.png" width="20"/></a>
            <a onclick="return confirm('¿Seguro/a que desea eliminar la categoría?')" href="/tp/inventario-istea/categoria/eliminar.php?id=<?php echo $categoria["id"]; ?>"><img src="/tp/inventario-istea/iconos/borrar.png" width="20"/></a>
        </td>
    </tr>
    <?php } ?>
</table>

<?php
include("../footer.php");
?>
