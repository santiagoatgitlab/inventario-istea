<?php
include("../header.php");

$search_line    = "";
$search_entry   = "";
if ( isset($_GET['search']) ){
    $search_entry = mysqli_real_escape_string ($dblink,$_GET["search"]);
    $search_line = ' where p.nombre like "%' . $search_entry . '%" or p.descripcion like "%' . $search_entry . '%"';
}

$query = "select p.id,p.nombre,p.descripcion,s.nombre as subcategoria,c.nombre as categoria,pv.nombre as proveedor,pc.precio,m.simbolo as moneda
    from productos p
    left join subcategoria s on s.id = p.subcategoria_id
    left join categoria c on c.id = s.categoria_id
    left join proveedores pv on pv.id = p.proveedor_id
    left join precio pc on pc.id = p.precio_id
    left join moneda m on m.id = pc.moneda_id
    " . $search_line;

$qry_result = mysqli_query  ($dblink,$query);

$productos = mysqli_fetch_all ($qry_result,MYSQLI_ASSOC); //extrae el array con los datos de la DB
if (is_null($productos)){
    $productos = [];
}
?>


<h1>Productos</h1>
<?php if (!empty($search_entry)) { ?>
<div class="search-results">
    <p>Resultados de la búsqueda: <span>"<?php echo $search_entry; ?>"</span></p>
    <a href="listar.php">X</a>
</div>
<?php } ?>
<nav class="top">
    <a href="/tp/inventario-istea/productos/insert.php">Nuevo producto</a>
    <form action="" method="get">
        <input type="text" name="search" />
        <input type="submit" value="Buscar" />
    </form>
</nav>
<table cellspacing="0" cellpadding="0">
    <tr> <!-- abrir fila -->
        <th>ID</th>   <!-- columna -->
        <th>Nombre</th>
        <th>Descripción</th>
        <th>Subcategoría</th>
        <th>Proveedor</th>
        <th>Precio</th>
        <th>Acciones</th>
    </tr> <!-- cerrar fila -->
    <?php foreach ($productos as $producto) { ?>
    <tr>
        <td><?php echo $producto["id"]; ?></td>
        <td><?php echo $producto["nombre"]; ?></td>
        <td><?php echo $producto["descripcion"]; ?></td>
        <td><?php echo $producto["subcategoria"]; ?></td>
        <td><?php echo $producto["proveedor"]; ?></td>
        <td><?php echo $producto["moneda"] . " " . $producto["precio"]; ?></td>
        <td>
            <a href="/tp/inventario-istea/productos/editar.php?id=<?php echo $producto["id"]; ?>"><img src="/tp/inventario-istea/iconos/editar.png" width="20"/></a>
            <a onclick="return confirm('¿Seguro/a que desea eliminar el producto?')" href="/tp/inventario-istea/productos/eliminar.php?id=<?php echo $producto["id"]; ?>"><img src="/tp/inventario-istea/iconos/borrar.png" width="20"/></a>
        </td>
    </tr>
    <?php } ?>
</table>
<?php
include("../footer.php");
?>
