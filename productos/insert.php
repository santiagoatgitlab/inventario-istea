<?php
include("../header.php");
?>

<h2>Nuevo producto</h2>

<?php

if (!isset($_POST["nombre"])){

$qry_result     = mysqli_query($dblink, "select s.id,s.nombre,c.nombre as categoria
    from subcategoria s
    left join categoria c on c.id = s.categoria_id
    order by c.nombre,s.nombre
    ");
$subcategorias  = mysqli_fetch_all($qry_result,MYSQLI_ASSOC);

$qry_result     = mysqli_query($dblink, "select id,Nombre from proveedores");
$proveedores    = mysqli_fetch_all($qry_result,MYSQLI_ASSOC);

$qry_result     = mysqli_query($dblink, "select id,nombre,simbolo from moneda");
$monedas        = mysqli_fetch_all($qry_result,MYSQLI_ASSOC);

?>

<form action="" method="POST">
    <div class="campo">
        <label>Nombre</label>
        <input type="text" name="nombre" required />
    </div>
    <div class="campo">
        <label>Descripcion</label>
        <textarea name="descripcion" required></textarea>
    </div>
    <div class="campo">
        <label>Categoría</label>
		<select name="subcategoria" required>
		<?php foreach ($subcategorias as $subcategoria) { ?> 
			<option value="<?php echo $subcategoria["id"]; ?>"><?php echo $subcategoria["categoria"] . " - " . $subcategoria["nombre"]; ?></option>
		<?php } ?>
		</select>
    </div>
    <div class="campo">
        <label>Proveedor</label>
		<select name="proveedor" required>
		<?php foreach ($proveedores as $proveedor) { ?> 
			<option value="<?php echo $proveedor["id"]; ?>"><?php echo $proveedor["Nombre"]; ?></option>
		<?php } ?>
		</select>
    </div>
    <div class="campo">
        <label>Precio</label>
        <input type="text" name="precio" />
		<select name="moneda" required>
		<?php foreach ($monedas as $moneda) { ?> 
			<option value="<?php echo $moneda["id"]; ?>"><?php echo $moneda["nombre"] . "(" . $moneda["simbolo"] . ")"; ?></option>
		<?php } ?>
		</select>
    </div>
    <input type="submit" value="Enviar"/>
</form>
<?php }
else {
    $nombre 	        = mysqli_real_escape_string ($dblink,$_POST["nombre"]);
    $descripcion        = mysqli_real_escape_string ($dblink,$_POST["descripcion"]);
    $subcategoria_id    = mysqli_real_escape_string ($dblink,$_POST["subcategoria"]);
    $proveedor_id       = mysqli_real_escape_string ($dblink,$_POST["proveedor"]);
    $precio 		    = mysqli_real_escape_string ($dblink,$_POST["precio"]);
    $moneda_id		    = mysqli_real_escape_string ($dblink,$_POST["moneda"]);

    // inserto precio si no existe
    $query_precio = "insert into precio (precio,moneda_id) values ($precio,$moneda_id)";
    $precio_id = 0;
    if (mysqli_query($dblink,$query_precio)){
        $precio_id = mysqli_insert_id($dblink);
    }
    else{
        $query_precio   = "select id from precio where precio = $precio and moneda_id = $moneda_id";
        $precio_result  = mysqli_query($dblink,$query_precio);
        $precio_id      = mysqli_fetch_array($precio_result,MYSQLI_ASSOC)['id'];
    }

    $query = "insert into productos (nombre,descripcion,subcategoria_id,proveedor_id,precio_id) 
					values (
					'$nombre',
					'$descripcion',
					$subcategoria_id,
					$proveedor_id,
					$precio_id
					)";

    //echo $query . "</br>";

    $qry_result = mysqli_query  ($dblink,$query);
    if ($qry_result) {
        echo "Se creó el nuevo producto";
    } else {
        echo "Hubo un error, no se creó el producto";
    }
}
?>
<nav>
    <a href="/tp/inventario-istea/productos/listar.php">Volver al listado</a>
</nav>
<?php
include("../footer.php");
?>
