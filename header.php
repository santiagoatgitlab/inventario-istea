<?php 
header('Content-type: text/html; charset=utf-8');
include("../conexion.php");
?>
<html>
<head>
    <meta charset="utf-8"/>
	<link rel="stylesheet" href="/tp/inventario-istea/style.css"/>
</head>
<body>
	<div>
		<header>
			<h1>INVENTARIO ISTEA 2020</h1>
		</header>
		<div class="content">
			<div class="col-left">
				<ul>
					<li><a href="/tp/inventario-istea/categoria/listar.php">Categorías</a></li>
					<li><a href="/tp/inventario-istea/subcategorias/listar.php">Subcategorías</a></li>
					<li><a href="/tp/inventario-istea/moneda/listar.php">Monedas</a></li>
					<li><a href="/tp/inventario-istea/precios/listar.php">Precios</a></li>
					<li><a href="/tp/inventario-istea/tipos/listar.php">Tipos de proveedores</a></li>
					<li><a href="/tp/inventario-istea/proveedores/listar.php">Proveedores</a></li>
					<li><a href="/tp/inventario-istea/productos/listar.php">Productos</a></li>
				</ul>
			</div>
			<main>
