<?php
include("../header.php");

$search_line    = "";
$search_entry   = "";
if ( isset($_GET['search']) ){
    $search_entry = mysqli_real_escape_string ($dblink,$_GET["search"]);
    $search_line = ' where nombre like "%' . $search_entry . '%"';
}

$query = "select * from tipos" . $search_line;
$qry_result = mysqli_query  ($dblink,$query);
$tipos = mysqli_fetch_all ($qry_result,MYSQLI_ASSOC); //extrae el array con los datos de la DB
?>


<h1>Tipos de proveedores</h1>
<?php if (!empty($search_entry)) { ?>
<div class="search-results">
    <p>Resultados de la búsqueda: <span>"<?php echo $search_entry; ?>"</span></p>
    <a href="listar.php">X</a>
</div>
<?php } ?>
<nav class="top">
    <a href="/tp/inventario-istea/tipos/insert.php">Nuevo tipo de proveedor</a>
    <form action="" method="get">
        <input type="text" name="search" />
        <input type="submit" value="Buscar" />
    </form>
</nav>
<table cellspacing="0" cellpadding="0">
    <tr> <!-- abrir fila -->
        <th>ID</th>   <!-- columna -->
        <th>Nombre</th>
        <th>Acciones</th>
    </tr> <!-- cerrar fila -->
    <?php foreach ($tipos as $tipo) { ?>
    <tr>
        <td><?php echo $tipo["id"]; ?></td>
        <td><?php echo $tipo["Nombre"]; ?></td>
        <td>
            <a href="/tp/inventario-istea/tipos/editar.php?id=<?php echo $tipo["id"]; ?>"><img src="/tp/inventario-istea/iconos/editar.png" width="20"/></a>
            <a onclick="return confirm('¿Seguro/a que desea eliminar el tipo de proveedor?')" href="/tp/inventario-istea/tipos/eliminar.php?id=<?php echo $tipo["id"]; ?>"><img src="/tp/inventario-istea/iconos/borrar.png" width="20"/></a>
        </td>
    </tr>
    <?php } ?>
</table>

<?php
include("../footer.php");
?>
