<?php
include("../header.php");
?>

<h1>Editar tipo de proveedor</h1>

<?php
    $id         = mysqli_real_escape_string ($dblink,$_GET["id"]);
if (!isset($_POST["nombre"])){
    $query      = "select * from tipos where id = $id";
    $qry_result = mysqli_query  ($dblink,$query);
    $tipos    = mysqli_fetch_array ($qry_result,MYSQLI_ASSOC);
?>

<form action="" method="POST"> <!-- atributo action indica a donde enviar la informacion del formulario -->
    <div>
        <label>Nombre</label>
        <input type="text" name="nombre" value="<?php echo $tipos["Nombre"]; ?>" required />
    </div>
    <input type="submit" value="Enviar"/>
</form>
<?php }
else {
    $nombre     = mysqli_real_escape_string ($dblink,$_POST["nombre"]);
    $query      = "update tipos set Nombre = '$nombre' where id = $id";
    /*echo $query . "</br>";*/
    $qry_result = mysqli_query  ($dblink,$query);
    if ($qry_result) {
        echo "Se actualizó el tipo de proveedor";
    } else {
        echo "Hubo un error, no se actualizó el tipo de proveedor";
    }
}
?>

<nav>
    <a href="/tp/inventario-istea/tipos/listar.php">Volver al listado</a>
</nav>
<?php
include("../footer.php");
?>
