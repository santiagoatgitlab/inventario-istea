<?php
include("../header.php");
?>

<h1>Nuevo tipo de proveedor</h1>

<?php

if (!isset($_POST["nombre"])){
?>

<form action="" method="POST">
    <div>
        <label>Nombre</label>
        <input type="text" name="nombre" required />
    </div>
    <input type="submit" value="Enviar"/>
</form>
<?php }
else {
    $nombre     = mysqli_real_escape_string ($dblink,$_POST["nombre"]);
    $query      = "insert into tipos (nombre) values ('$nombre')";
    /*echo $query . "</br>";*/
    $qry_result = mysqli_query  ($dblink,$query);
    if ($qry_result) {
        echo "Se creó el nuevo tipo de proveedor";
    } else {
        echo "Hubo un error, no se creó el nuevo tipo de proveedor";
    }
}
?>

<nav>
    <a href="/tp/inventario-istea/tipos/listar.php">Volver al listado</a>
</nav>
<?php
include("../footer.php");
?>
