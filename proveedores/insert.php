<?php
include("../header.php");
?>

<h2>Nuevo proveedor</h2>

<?php

if (!isset($_POST["nombre"])){
$query = "select p.id,p.nombre,p.apellido,p.cuit,p.direccion,p.telefono,tipo_id from proveedores p";

$qry_result = mysqli_query($dblink, "select id,Nombre from tipos");
$tipos = mysqli_fetch_all($qry_result,MYSQLI_ASSOC);
?>

<form action="" method="POST">
    <div class="campo">
        <label>Nombre</label>
        <input type="text" name="nombre" required />
    </div>
    <div class="campo">
        <label>Apellido</label>
        <input type="text" name="apellido" required />
    </div>
    <div class="campo">
        <label>Cuit</label>
        <input type="text" name="cuit" required />
    </div>
    <div class="campo">
        <label>Dirección</label>
        <input type="text" name="direccion" required />
    </div>
    <div class="campo">
        <label>Teléfono</label>
        <input type="text" name="telefono" required />
    </div>
    <div class="campo">
        <label>Tipo</label>
		<select name="tipo" required>
		<?php foreach ($tipos as $tipo) { ?> 
			<option value="<?php echo $tipo["id"]; ?>"><?php echo $tipo["Nombre"]; ?></option>
		<?php } ?>
		</select>
    </div>
	</br>
    <input type="submit" value="Enviar"/>
</form>
<?php }
else {
    $nombre 	= mysqli_real_escape_string ($dblink,$_POST["nombre"]);
    $apellido 	= mysqli_real_escape_string ($dblink,$_POST["apellido"]);
    $cuit 		= mysqli_real_escape_string ($dblink,$_POST["cuit"]);
    $direccion 	= mysqli_real_escape_string ($dblink,$_POST["direccion"]);
    $telefono 	= mysqli_real_escape_string ($dblink,$_POST["telefono"]);
    $tipo 		= mysqli_real_escape_string ($dblink,$_POST["tipo"]);
    $query 		= "insert into proveedores (nombre,apellido,cuit,direccion,telefono,tipo_id) 
					values (
					'$nombre',
					'$apellido',
					'$cuit',
					'$direccion',
					'$telefono',
					$tipo
					)";
    //echo $query . "</br>";
    $qry_result = mysqli_query  ($dblink,$query);
    if ($qry_result) {
        echo "Se creó el nuevo proveedor";
    } else {
        echo "Hubo un error, no se creó el proveedor";
    }
}
?>
<nav>
    <a href="/tp/inventario-istea/proveedores/listar.php">Volver al listado</a>
</nav>
<?php
include("../footer.php");
?>
