<?php
include("../header.php");

$search_line    = "";
$search_entry   = "";
if ( isset($_GET['search']) ){
    $search_entry = mysqli_real_escape_string ($dblink,$_GET["search"]);
    $search_line = ' where p.nombre like "%' . $search_entry . '%" 
                    or p.apellido like "%' . $search_entry . '%"
                    or p.cuit like "%' . $search_entry . '%"
                    or p.direccion like "%' . $search_entry . '%"
                    or p.telefono like "%' . $search_entry . '%"
                    ';
}

$query = "select p.id,p.nombre,p.apellido,p.cuit,p.direccion,p.telefono,t.nombre as tipo
    from proveedores p
    left join tipos t on t.id = p.tipo_id
    " . $search_line;

$qry_result = mysqli_query  ($dblink,$query);
$proveedores = mysqli_fetch_all ($qry_result,MYSQLI_ASSOC); //extrae el array con los datos de la DB

if (is_null($proveedores)){
    $proveedores = [];
}
?>


<h1>Proveedores</h1>
<?php if (!empty($search_entry)) { ?>
<div class="search-results">
    <p>Resultados de la búsqueda: <span>"<?php echo $search_entry; ?>"</span></p>
    <a href="listar.php">X</a>
</div>
<?php } ?>
<nav class="top">
    <a href="/tp/inventario-istea/proveedores/insert.php">Nuevo proveedor</a>
    <form action="" method="get">
        <input type="text" name="search" />
        <input type="submit" value="Buscar" />
    </form>
</nav>
<table cellspacing="0" cellpadding="0">
    <tr> <!-- abrir fila -->
        <th>ID</th>   <!-- columna -->
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Cuit</th>
        <th>Dirección</th>
        <th>Teléfono</th>
        <th>Tipo</th>
        <th>Acciones</th>
    </tr> <!-- cerrar fila -->
    <?php foreach ($proveedores as $proveedor) { ?>
    <tr>
        <td><?php echo $proveedor["id"]; ?></td>
        <td><?php echo $proveedor["nombre"]; ?></td>
        <td><?php echo $proveedor["apellido"]; ?></td>
        <td><?php echo $proveedor["cuit"]; ?></td>
        <td><?php echo $proveedor["direccion"]; ?></td>
        <td><?php echo $proveedor["telefono"]; ?></td>
        <td><?php echo $proveedor["tipo"]; ?></td>
        <td>
            <a href="/tp/inventario-istea/proveedores/editar.php?id=<?php echo $proveedor["id"]; ?>"><img src="/tp/inventario-istea/iconos/editar.png" width="20"/></a>
            <a onclick="return confirm('¿Seguro/a que desea eliminar el proveedor?')" href="/tp/inventario-istea/proveedores/eliminar.php?id=<?php echo $proveedor["id"]; ?>"><img src="/tp/inventario-istea/iconos/borrar.png" width="20"/></a>
        </td>
    </tr>
    <?php } ?>
</table>
<?php
include("../footer.php");
?>
