<?php
include("../header.php");
?>

<h2>Editar proveedor</h2>

<?php

$id 	= mysqli_real_escape_string ($dblink,$_GET["id"]);
if (!isset($_POST["nombre"])){
$query = "select id,nombre,apellido,cuit,direccion,telefono,tipo_id
    from proveedores 
    where id = $id";
$qry_result = mysqli_query  ($dblink,$query);
$proveedor  = mysqli_fetch_array ($qry_result,MYSQLI_ASSOC); //extrae el array con los datos de la DB
        
$qry_result = mysqli_query($dblink, "select id,Nombre from tipos");
$tipos = mysqli_fetch_all($qry_result,MYSQLI_ASSOC);
?>

<form action="" method="POST">
    <div class="campo">
        <label>Nombre</label>
        <input type="text" name="nombre" required value="<?php echo $proveedor['nombre']; ?>"/>
    </div>
    <div class="campo">
        <label>Apellido</label>
        <input type="text" name="apellido" required value="<?php echo $proveedor['apellido']; ?>"/>
    </div>
    <div class="campo">
        <label>Cuit</label>
        <input type="text" name="cuit" required value="<?php echo $proveedor['cuit']; ?>"/>
    </div>
    <div class="campo">
        <label>Dirección</label>
        <input type="text" name="direccion" required value="<?php echo $proveedor['direccion']; ?>"/>
    </div>
    <div class="campo">
        <label>Teléfono</label>
        <input type="text" name="telefono" required value="<?php echo $proveedor['telefono']; ?>"/>
    </div>
    <div class="campo">
        <label>Tipo</label>
		<select name="tipo" required>
		<?php foreach ($tipos as $tipo) { ?> 
			<option value="<?php echo $tipo["id"]; ?>"<?php if ($proveedor["tipo_id"] == $tipo["id"]) echo " selected"; ?>>
                <?php echo $tipo["Nombre"]; ?>
            </option>] 
		<?php } ?>
		</select>
    </div>
	</br>
    <input type="submit" value="Enviar"/>
</form>
<?php }
else {
    $nombre 	= mysqli_real_escape_string ($dblink,$_POST["nombre"]);
    $apellido 	= mysqli_real_escape_string ($dblink,$_POST["apellido"]);
    $cuit 		= mysqli_real_escape_string ($dblink,$_POST["cuit"]);
    $direccion 	= mysqli_real_escape_string ($dblink,$_POST["direccion"]);
    $telefono 	= mysqli_real_escape_string ($dblink,$_POST["telefono"]);
    $tipo_id    = mysqli_real_escape_string ($dblink,$_POST["tipo"]);
    $query 		= "update proveedores set
        nombre = '$nombre',
        apellido = '$apellido',
        cuit = '$cuit',
        direccion = '$direccion',
        telefono = '$telefono',
        tipo_id = $tipo_id
        where id = $id
    ";
    //echo $query . "</br>";
    $qry_result = mysqli_query  ($dblink,$query);
    if ($qry_result) {
        echo "Se actualizó el proveedor";
    } else {
        echo "Hubo un error, no se actualizó el proveedor";
    }
}
?>
<nav>
    <a href="/tp/inventario-istea/proveedores/listar.php">Volver al listado</a>
</nav>
<?php
include("../footer.php");
?>

