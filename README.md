**Pasos para instalación**

* Instalar wamp server  
* Iniciar los servicios de mysql y apache desde el panel de control de wamp  
* En el directorio donde se instaló wamp, ir al la carpeta http y crear dentro una carpeta llamada tp  
* Si sabes como clonar proyectos de git  
   clonar dentro del directorio tp el proyecto https://gitlab.com/santiagoatgitlab/inventario-istea.git  
* Si no  
   descomprimir al proyecto del zip en el directorio tp  
* Luego de estos pasos nos debería quedar la siguiente estructura: wamp/http/tp/inventario-istea  
* Usar la herramienta import de phpmyadmin para importar la base de datos que se encuentra en inventario-istea/database/estructura.sql
* Crear un usuario llamado 'saneze' y darle permisos para acceder a la base de datos inventario mediante las siguientes consulta sql  
   create user saneze;  
   grant all privileges on inventario.\* to 'saneze' identified by 'martes1a';  
* Ingresar mediante los navegadores Firefox o Brave a la url 'localhost/tp/inventario-istea'  
  
El proyecto debería andar. Si no anda contactese con santiagogonzalezrojo@yandex.ru para soporte  
  
**Entregables**
  
* Script base de datos    : 'inventario-istea/database/estructura.sql'  
* Script datos            : 'inventario-istea/database/datos.sql'  
* DER                     : 'inventario-istea/DER inventario.pdf'  

**Autores**

* Ezequiel Macchi  
* Santiago González Rojo  
