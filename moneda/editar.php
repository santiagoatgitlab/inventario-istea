<?php
include("../header.php");
?>

<h1>Monedas</h1>

<?php
    $id         = mysqli_real_escape_string ($dblink,$_GET["id"]);
if (!isset($_POST["nombre"])){
    $query      = "select * from moneda where id = $id";
    $qry_result = mysqli_query  ($dblink,$query);
    $monedas    = mysqli_fetch_array ($qry_result,MYSQLI_ASSOC);
?>

<form action="" method="POST"> <!-- atributo action indica a donde enviar la informacion del formulario -->
    <div>
        <label>Nombre</label>
        <input type="text" name="nombre" value="<?php echo $monedas["nombre"]; ?>" required />
    </div>
    <div>
        <label>Símbolo</label>
        <input type="text" name="simbolo" value="<?php echo $monedas["simbolo"]; ?>" required maxlength="3"/>
    </div>
    <input type="submit" value="Enviar"/>
</form>
<?php }
else {
    $nombre     = mysqli_real_escape_string ($dblink,$_POST["nombre"]);
    $simbolo    = mysqli_real_escape_string ($dblink,$_POST["simbolo"]);
    $query      = "update moneda set nombre = '$nombre', simbolo = '$simbolo' where id = $id";
    /*echo $query . "</br>";*/
    $qry_result = mysqli_query  ($dblink,$query);
    if ($qry_result) {
        echo "Se actualizó la moneda";
    } else {
        echo "Hubo un error, no se actualizó la moneda";
    }
}
?>

<nav>
    <a href="/tp/inventario-istea/moneda/listar.php">Volver al listado</a>
</nav>
<?php
include("../footer.php");
?>
