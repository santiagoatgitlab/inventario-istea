<?php
include("../header.php");
?>

<h1>Monedas</h1>

<?php

if (!isset($_POST["nombre"])){
?>

<form action="" method="POST">
    <div>
        <label>Nombre</label>
        <input type="text" name="nombre" required />
    </div>
    <div>
        <label>Símbolo</label>
        <input type="text" name="simbolo" required maxlength="3"/>
    </div>
    <input type="submit" value="Enviar"/>
</form>
<?php }
else {
    $nombre     = mysqli_real_escape_string ($dblink,$_POST["nombre"]);
    $simbolo    = mysqli_real_escape_string ($dblink,$_POST["simbolo"]);
    $query      = "insert into moneda (nombre,simbolo) values ('$nombre','$simbolo')";
    /*echo $query . "</br>";*/
    $qry_result = mysqli_query  ($dblink,$query);
    if ($qry_result) {
        echo "Se creó la nueva moneda";
    } else {
        echo "Hubo un error, no se creó la moneda";
    }
}
?>

<nav>
    <a href="/tp/inventario-istea/moneda/listar.php">Volver al listado</a>
</nav>
<?php
include("../footer.php");
?>
