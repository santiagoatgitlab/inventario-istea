<?php
include("../header.php");
?>

<h2>Editar subcategoria</h2>

<?php



$id = mysqli_real_escape_string($dblink,$_GET["id"]);



if (!isset($_POST["nombre"])){
$query = "select s.id,s.nombre,c.nombre as categoria, categoria_id
    from subcategoria s
    left join categoria c on c.id = s.categoria_id
    where s.id = $id
    order by s.id
    ";
            
$qry_result = mysqli_query  ($dblink,$query);
$subcategoria = mysqli_fetch_array ($qry_result,MYSQLI_ASSOC); //extrae el array con los datos de la DB

$query      = "select id,nombre from categoria";
$qry_result = mysqli_query($dblink,$query);
$categorias    = mysqli_fetch_all($qry_result,MYSQLI_ASSOC);
?>

<form action="" method="POST">
    <div class="campo">
        <label>Nombre</label>
        <input type="text" name="nombre" required value="<?php echo $subcategoria["nombre"]; ?>"/>
    </div>
    <div class="campo">
        <label>Categoría</label>
		<select name="categoria" required>
		<?php foreach ($categorias as $categoria) { ?> 
			<option value="<?php echo $categoria["id"]; ?>"<?php if ($subcategoria["categoria_id"] == $categoria['id']) echo " selected"; ?>>
                <?php echo $categoria["nombre"]; ?>
            </option>
		<?php } ?>
		</select>
    </div>
	</br>
    <input type="submit" value="Enviar"/>
</form>
<?php }
else {
    $subcategoria   = mysqli_real_escape_string ($dblink,$_POST["nombre"]);
    $categoria_id	= mysqli_real_escape_string ($dblink,$_POST["categoria"]);
    $query          = "update subcategoria set
            nombre          = '$subcategoria',
            categoria_id    = $categoria_id
            where id        = $id
        ";
    /*echo $query . "</br>";*/
    $qry_result = mysqli_query($dblink,$query);
    if ($qry_result) {
        echo "Se actualizó la subcategoría";
    } else {
        echo "Hubo un error, no se actualizó la subcategoría";
    }
}
?>

<nav>
    <a href="/tp/inventario-istea/subcategorias/listar.php">Volver al listado</a>
</nav>
<?php
include("../footer.php");
?>
