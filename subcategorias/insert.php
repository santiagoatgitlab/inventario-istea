<?php
include("../header.php");
?>

<h2>Nueva subcategoría</h2>

<?php

if (!isset($_POST["nombre"])){
$query      = "select id,nombre from categoria";
$qry_result = mysqli_query($dblink,$query);
$categorias    = mysqli_fetch_all($qry_result,MYSQLI_ASSOC);
?>

<form action="" method="POST">
    <div class="campo">
        <label>Nombre</label>
        <input type="text" name="nombre" required />
    </div>
    <div class="campo">
        <label>Categoría</label>
		<select name="categoria" required>
		<?php foreach ($categorias as $categoria) { ?> 
			<option value="<?php echo $categoria["id"]; ?>"><?php echo $categoria["nombre"]; ?></option>
		<?php } ?>
		</select>
    </div>
	</br>
    <input type="submit" value="Enviar"/>
</form>
<?php }
else {
    $subcategoria   = mysqli_real_escape_string ($dblink,$_POST["nombre"]);
    $categoria_id   = mysqli_real_escape_string ($dblink,$_POST["categoria"]);
    $query          = "insert into subcategoria (nombre,categoria_id)
                        values (
                        '$subcategoria',
                        $categoria_id
                        )";
    //echo $query . "</br>";
    $qry_result = mysqli_query  ($dblink,$query);
    if ($qry_result) {
        echo "Se creó la subcategoría";
    } else {
        echo "Hubo un error, no se creó la subcategoría";
    }
}
?>

<nav>
    <a href="/tp/inventario-istea/subcategorias/listar.php">Volver al listado</a>
</nav>
<?php
include("../footer.php");
?>
