<?php
include("../header.php");

$search_line    = "";
$search_entry   = "";
if ( isset($_GET['search']) ){
    $search_entry = mysqli_real_escape_string ($dblink,$_GET["search"]);
    $search_line = ' where s.nombre like "%' . $search_entry . '%" or c.nombre like "%' . $search_entry . '%"';
}

$query = "select s.id,s.nombre,c.nombre as categoria
    from subcategoria s
    left join categoria c on c.id = s.categoria_id
    " . $search_line . " order by id";
$qry_result = mysqli_query  ($dblink,$query);
$subcategorias = mysqli_fetch_all ($qry_result,MYSQLI_ASSOC); //extrae el array con los datos de la DB

if (is_null($subcategorias)){
    $subcategorias = [];
}
?>

<h1>Subcategorías</h1>
<?php if (!empty($search_entry)) { ?>
<div class="search-results">
    <p>Resultados de la búsqueda: <span>"<?php echo $search_entry; ?>"</span></p>
    <a href="listar.php">X</a>
</div>
<?php } ?>
<nav class="top">
    <a href="/tp/inventario-istea/subcategorias/insert.php">Nueva subcategoría</a>
    <form action="" method="get">
        <input type="text" name="search" />
        <input type="submit" value="Buscar" />
    </form>
</nav>
<table cellspacing="0" cellpadding="0">
    <tr> <!-- abrir fila -->
        <th>ID</th>   <!-- columna -->
        <th>Nombre</th>
        <th>Categoría</th>
        <th>Acciones</th>
    </tr> <!-- cerrar fila -->
    <?php foreach ($subcategorias as $subcategoria) { ?>
    <tr>
        <td><?php echo $subcategoria["id"]; ?></td>
        <td><?php echo $subcategoria["nombre"]; ?></td>
        <td><?php echo $subcategoria["categoria"]; ?></td>
        <td>
            <a href="/tp/inventario-istea/subcategorias/editar.php?id=<?php echo $subcategoria["id"]; ?>"><img src="/tp/inventario-istea/iconos/editar.png" width="20"/></a>
            <a onclick="return confirm('¿Seguro/a que desea eliminar la subcategoría?')" href="/tp/inventario-istea/subcategorias/eliminar.php?id=<?php echo $subcategoria["id"]; ?>"><img src="/tp/inventario-istea/iconos/borrar.png" width="20"/></a>
        </td>
    </tr>
    <?php } ?>
</table>
<?php
include("../footer.php");
?>
