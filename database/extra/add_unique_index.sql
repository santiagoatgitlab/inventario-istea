alter table moneda add constraint unique_currency UNIQUE (nombre);
alter table categoria add constraint unique_category UNIQUE (nombre);
alter table subcategoria add constraint unique_subcategory UNIQUE (nombre);
alter table precio add constraint unique_price UNIQUE (precio,moneda_id);
alter table productos add constraint unique_product UNIQUE (nombre);
alter table proveedores add constraint unique_provider UNIQUE (nombre);
alter table tipos add constraint unique_type UNIQUE (nombre);
