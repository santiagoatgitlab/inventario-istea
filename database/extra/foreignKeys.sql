ALTER TABLE `productos` ADD FOREIGN KEY ( `precio` ) REFERENCES `inventario`.`precio` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;
ALTER TABLE `subcategoria` ADD FOREIGN KEY ( `categoria` ) REFERENCES `inventario`.`categoria` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;
ALTER TABLE `precio` ADD FOREIGN KEY ( `moneda` ) REFERENCES `inventario`.`moneda` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;
ALTER TABLE `proveedores` ADD FOREIGN KEY ( `tipo` ) REFERENCES `inventario`.`tipos` (
`id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;