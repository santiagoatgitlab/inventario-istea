-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: inventario
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (15,'Adornaciones'),(10,'Alimentos'),(18,'Arqueología'),(7,'Hogar y Jardín'),(6,'Juguetería'),(11,'Mascotas'),(16,'Moda y accesorios'),(17,'Vehículos');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `moneda`
--

LOCK TABLES `moneda` WRITE;
/*!40000 ALTER TABLE `moneda` DISABLE KEYS */;
INSERT INTO `moneda` VALUES (1,'Dólar','U$S'),(2,'Peso','$'),(3,'Euro','EUR'),(7,'Sol','$OL'),(9,'Yen','Y$Y');
/*!40000 ALTER TABLE `moneda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `precio`
--

LOCK TABLES `precio` WRITE;
/*!40000 ALTER TABLE `precio` DISABLE KEYS */;
INSERT INTO `precio` VALUES (23,14,2),(3,14.5,1),(32,15,2),(4,50,7),(5,99,1),(10,123,1),(28,145,2),(16,251,2),(13,445,1),(29,445,2),(18,500,1),(2,1000,2),(9,1450,2),(12,34324,1),(26,23489800,1);
/*!40000 ALTER TABLE `precio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,'Gato persa Johnny','Johhny tiene 5 meses y 13450 pelos. de los cuales pierde 14 por día',7,3,3),(2,'Silla plástica Solymar','Silla de plástico plegable para el jardin',3,2,4),(4,'Camión de bomberos DURAVIT','Camión de bomberos, no incluye incendio ni periodistas',5,5,9),(8,'Perro Boyero de Berna','Perrito recién nacido. Come mucho y puede llegar a medir 5 metros, guarda',9,2,18),(9,'Gorrion Vuela Vuela','No le hace falta equipaje, tiene plumas. Odia a los gatos',10,7,18),(10,'Pintura pared','Color piel, mas o menos',16,2,23),(12,'Locomotora','Locomotora a pilas. Velocidad máxima 780km/h',5,3,28),(13,'Osito Felpuche','Felpa o peluche, fiel, tranquilo. No come mucho, no come nada',17,3,29),(14,'Cuadro las meninas','Las meninas, original, velazquez.',16,7,18),(15,'Craneo Austrolopitecus','Craneo de austrolopitecus de 861.000 años de antigüedad encontrado en el sur de Irán',18,3,32);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` VALUES (2,'Rabino','Molone','21344452232','Molone 541','43495966',3),(3,'Customa','Morigerant','30445254541','Laloma 14','39939292',3),(5,'Reiko','Mantenot','423030431032','La paloma 54','11-3435-5456',1),(6,'Cosmetica','Lingüal','204343450','Cabildo 993','15-3334-5543',5),(7,'shaco','la justicia','111311141','Patrul 9071','11.5465.7715',3);
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `subcategoria`
--

LOCK TABLES `subcategoria` WRITE;
/*!40000 ALTER TABLE `subcategoria` DISABLE KEYS */;
INSERT INTO `subcategoria` VALUES (1,'Comida china',10),(2,'Mesas',7),(3,'Sillas',7),(4,'Dinosaurios',6),(5,'Autos',6),(6,'Barbie y ken',6),(7,'Gatos',11),(8,'Flores',15),(9,'Perros',11),(10,'Pajaritos',11),(11,'Mariscos',10),(13,'Automóvil',17),(16,'Interior',7),(17,'Peluches',6),(18,'Homínidas',18);
/*!40000 ALTER TABLE `subcategoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tipos`
--

LOCK TABLES `tipos` WRITE;
/*!40000 ALTER TABLE `tipos` DISABLE KEYS */;
INSERT INTO `tipos` VALUES (2,'Mala onda'),(5,'Persona8'),(3,'Que hace chistes'),(1,'Que viene con un camión');
/*!40000 ALTER TABLE `tipos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-23 20:51:20
